#pragma once
#include "Utilities.h"
class Evidence_Grid
{
private:
	enum State {OPEN = 0 ,UNKNOWN  = 1,OCCUPIED = 2};
	State ** m_grid;
	int m_height;
	int m_width;

public:
	Evidence_Grid(void);
	Evidence_Grid(int h,int w);
	int getHeight()const  { return m_height ;}
	int getWidth()const  { return m_width ;}
	State getNode(int p_X,int p_Y) const;
	void setNode(int p_X,int p_Y,int p_nodeState);
	bool checkBoundries(const Point& p) const;
	bool isWalkable(const Point& p) const;
	bool isUnknown(const Point& p) const;
	bool isFrontier(const Point& p) const;
	vector<Point> detectFrontierNodes() const;
	State ** getOccupanyGrid() const { return m_grid;}
	~Evidence_Grid(void);
	void Clear(void);
};

