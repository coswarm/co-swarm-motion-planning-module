#pragma once
#include "PathPlanner.h"
#include <windows.h>
using namespace System;
using namespace System::Collections;


public ref class AreaExploration
{
private:
	Evidence_Grid * m_EvidenceGrid;
	PathPlanner * m_MotionPlanner;
	int m_RobotPositionX;
	int m_RobotPositionY;
public:
	AreaExploration(void);
	AreaExploration(int h,int w,int p_RobotPositionX,int p_RobotPositionY);
	Generic::List<Drawing::Point>^ getPath_of_Next_Destination();
	void setRobotPosition(int p_RobotPositionX,int p_RobotPositionY);
	Drawing::Point^ getRobotPosition();
	array<int,2>^ getGlobalGrid();
	void addLocalGrid(array<int,2>^ p_localGrid,int p_centerPointX,int p_centerPointY,int p_localGrid_Height,int p_localGrid_Width);
};

