#pragma once
#include "Evidence_Grid.h"
#include "BinaryHeap.h"
using namespace std;

class PathPlanner
{
private:
	Evidence_Grid* m_occupancyGrid;
	Node ** m_pathGrid;
	bool ** m_visited;
	BinaryHeap< pair<Node,Point> > m_openList;
	int H(Point p1,Point p2);
	void InitializePathPlanner(Point p_startPoint);
	int Find_ShortestPath_MIN_Cost(Point p_startPoint , Point p_endPoint);
	vector<Point> Get_Path(Point p_startPoint , Point p_endPoint);
	Point m_previousGoal;
public:
	PathPlanner(void);
	PathPlanner(Evidence_Grid* globalGrid);
	PathPlanner(Evidence_Grid* globalGrid,Point p_initialGoal);
	~PathPlanner(void);
	vector<Point> Get_ShortestPath(Point start_point,vector<Point> Goals);
	void Clear();
};

