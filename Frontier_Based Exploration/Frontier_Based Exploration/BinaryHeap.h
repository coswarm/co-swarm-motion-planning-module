#pragma once
#include "Utilities.h"

template<typename T>
class BinaryHeap
{
private:
	void heapify_subtree(int);
	void propagate_up(int);
	vector<T> HeapList;

public:
	BinaryHeap(void);
	BinaryHeap(int);
	~BinaryHeap(void);
	T top();
	T pop();
	void push(T);
	int find(T);
	bool is_empty();
	void update_element(int, T);
	void clear();
};

using namespace std;

#define PARENT(index) index / 2
#define LEFT_CHILD(index) 2 * index
#define RIGHT_CHILD(index) 2 * index + 1

template<typename T>
BinaryHeap<T>::BinaryHeap(void)
{
	HeapList= vector<T>();
}

template<typename T>
BinaryHeap<T>::BinaryHeap(int size)
{
	HeapList= vector<T>();
}

template<typename T>
BinaryHeap<T>::~BinaryHeap(void)
{
	this->clear();
}

template<typename T>
T BinaryHeap<T>::top()
{
	assert(!is_empty());		// Make sure that the heap contains at least one element
	return HeapList[0];
}

template<typename T>
bool BinaryHeap<T>::is_empty()
{
	return HeapList.empty();
}

template<typename T>
void BinaryHeap<T>::push(T newElement)
{
	HeapList.push_back(newElement); 
	push_heap (HeapList.begin(),HeapList.end(), greater<T>());
}

template<typename T>
T BinaryHeap<T>::pop()
{
	assert(!is_empty());		// Make sure that the heap contains at least one element
	pop_heap (HeapList.begin(),HeapList.end(), greater<T>());
	T TopElement = HeapList[HeapList.size()-1];
	HeapList.pop_back();
	return TopElement;
}

template<typename T>
int BinaryHeap<T>::find(T existObject)
{
	for(int i=0;i<HeapList.size();++i)
	{
		if(existObject==HeapList[i])
			return i;
	}
	return -1;
}

template<typename T>
void BinaryHeap<T>::update_element(int index , T element)
{
	assert(index >= 0 && index < HeapList.size());	// Make sure that a valid index is used. 
	HeapList[index]=element;
	make_heap(HeapList.begin(), HeapList.end(), greater<T>());
}

template<typename T>
void BinaryHeap<T>::heapify_subtree(int index)
{
	int leftIndex = LEFT_CHILD(index);
	int	rightIndex = RIGHT_CHILD(index);
	int smallestIndex = index;

	if (leftIndex < HeapList.size() && HeapList[leftIndex] < HeapList[index])
		smallestIndex = leftIndex;
	if (rightIndex < HeapList.size() && HeapList[rightIndex] < HeapList[smallestIndex])
		smallestIndex = rightIndex;

	if (smallestIndex != index)
	{
		swap(HeapList[smallestIndex], HeapList[index]);
		heapify_subtree(smallestIndex);
	}
}

// Compares index with parent and swaps node if larger O(log(n)) 
template<typename T>
void BinaryHeap<T>::propagate_up(int index)
{
	while (index > 1 && HeapList[PARENT(index)] > HeapList[index])
	{
		swap(HeapList[index], HeapList[PARENT(index)]);
		index = PARENT(index);
	}
}
template<typename T>
void BinaryHeap<T>::clear()
{
	HeapList.clear();
}