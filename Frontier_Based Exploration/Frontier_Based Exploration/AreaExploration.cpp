#include "AreaExploration.h"

AreaExploration::AreaExploration(void)
{
	m_EvidenceGrid = new Evidence_Grid(); 
	m_MotionPlanner = new PathPlanner(m_EvidenceGrid);
}
AreaExploration::AreaExploration(int h,int w,int p_RobotPositionX,int p_RobotPositionY)
{
	m_EvidenceGrid = new Evidence_Grid(h,w);
	m_MotionPlanner = new PathPlanner(m_EvidenceGrid,Point(p_RobotPositionX,p_RobotPositionY));
	setRobotPosition(p_RobotPositionX,p_RobotPositionY);
}
Generic::List<Drawing::Point>^ AreaExploration::getPath_of_Next_Destination()
{
	Point robotPos;

	robotPos.X = m_RobotPositionX;
	robotPos.Y = m_RobotPositionY;
	vector<Point> path = m_MotionPlanner->Get_ShortestPath(robotPos,m_EvidenceGrid->detectFrontierNodes());

	Generic::List<Drawing::Point>^ finalPath = gcnew Generic::List<Drawing::Point>();
	for(int index=0;index < path.size();++index)
		finalPath->Add(Drawing::Point(path[index].X,path[index].Y));

	return finalPath;
}
void AreaExploration::setRobotPosition(int p_RobotPositionX,int p_RobotPositionY)
{
	m_RobotPositionX=p_RobotPositionX;
	m_RobotPositionY=p_RobotPositionY;
}
Drawing::Point^ AreaExploration::getRobotPosition()
{
	return gcnew Drawing::Point(m_RobotPositionX,m_RobotPositionY); 
}
array<int,2>^ AreaExploration::getGlobalGrid()
{
	int global_Grid_Height = m_EvidenceGrid ->getHeight() ;
	int global_Grid_Width = m_EvidenceGrid ->getWidth() ;

	array<int,2>^ global_Grid = gcnew array<int,2>(global_Grid_Height,global_Grid_Width);
	int ** ptr_occupanyGrid = (int**)m_EvidenceGrid->getOccupanyGrid();

	for (int y=0;y<global_Grid_Height ; ++ y)
		for(int x=0;x<global_Grid_Width;++ x)
			global_Grid[y,x]=ptr_occupanyGrid[y][x];
	return global_Grid;
}
void AreaExploration::addLocalGrid(array<int,2>^ p_localGrid,int p_centerPointX,int p_centerPointY,int p_localGrid_Height,int p_localGrid_Width)
{
	int relative_StartX= p_centerPointX - (p_localGrid_Width/2);
	int relative_StartY= p_centerPointY - (p_localGrid_Height/2);
	int relative_CurrentX;
	for(int y=0;y<p_localGrid_Height;++y)
	{
		relative_CurrentX=relative_StartX;
		for(int x=0;x<p_localGrid_Width;++x)
			m_EvidenceGrid->setNode(relative_CurrentX ++,relative_StartY,p_localGrid[y,x]);
		++ relative_StartY;
	}
}