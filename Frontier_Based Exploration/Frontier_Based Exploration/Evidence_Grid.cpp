#include "Evidence_Grid.h"

Evidence_Grid::Evidence_Grid(void)
{
	Evidence_Grid(14,14);
}
Evidence_Grid::Evidence_Grid(int h,int w): m_height(h) ,m_width(w)
{
	m_grid = new State*[m_height];
	for(int row_index=0;row_index < m_height; ++ row_index)
		m_grid[row_index] = new State[m_width];

	for(int y = 0 ; y < m_height ; ++ y)
		for(int x=0;x < m_width; ++ x)
			m_grid[y][x] = State::UNKNOWN;

}
Evidence_Grid::State Evidence_Grid::getNode(int p_X,int p_Y) const
{
	return m_grid[p_Y][p_X]; 
}
void Evidence_Grid::setNode(int p_X,int p_Y,int p_nodeState)
{
	m_grid[p_Y][p_X]=(Evidence_Grid::State)p_nodeState;
}
vector<Point> Evidence_Grid::detectFrontierNodes() const
{
	vector<Point> frontier_points;
	Point cur_point;
	for(int y=0;y<m_height;++ y)
	{
		for(int x=0;x<m_width; ++ x)
			if(m_grid[y][x] == State::OPEN)
			{
				cur_point.X=x;
				cur_point.Y=y;
				if(isFrontier(cur_point))
					frontier_points.push_back(cur_point);
			}
	}
	return frontier_points;
}
bool Evidence_Grid::checkBoundries(const Point& p) const
{
	return p.X>=0 && p.X < m_width && p.Y>=0 && p.Y < m_height;
}
bool Evidence_Grid::isWalkable(const Point& p) const
{
	return m_grid[p.Y][p.X] == State::OPEN;
}
bool Evidence_Grid::isUnknown(const Point& p) const
{
	return m_grid[p.Y][p.X] == State::UNKNOWN;
}
bool Evidence_Grid::isFrontier(const Point& p) const
{
	Point adj_point;
	for(int index=0;index<POSSIBLE_MOVES_SIZE;++index)
	{
		adj_point.X=p.X + xDirection[index];
		adj_point.Y=p.Y + yDirection[index];
		if(checkBoundries(adj_point))
			if(isUnknown(adj_point))
				return true;
	}
	return false;
}
void Evidence_Grid::Clear(void)
{
	for(int row_index=0;row_index < m_height; ++ row_index)
		delete[] m_grid[row_index];
	delete m_grid;
}
Evidence_Grid::~Evidence_Grid(void)
{
	Clear();
}
