#include "PathPlanner.h"
using namespace std;

int PathPlanner::H(Point p1,Point p2)
{
	return abs(p1.X-p2.X) + abs(p1.Y-p2.Y);  
}
void PathPlanner::InitializePathPlanner(Point p_startPoint)
{
	for(int y=0;y<m_occupancyGrid->getHeight();++y)
		for(int x=0;x<m_occupancyGrid->getWidth();++x)
		{
			m_pathGrid[y][x].reset();
			m_visited[y][x]=false;
		}
		m_openList.clear();
		m_pathGrid[p_startPoint.Y][p_startPoint.X].setG(0);
		m_openList.push(make_pair(m_pathGrid[p_startPoint.Y][p_startPoint.X],p_startPoint));
}

int PathPlanner::Find_ShortestPath_MIN_Cost(Point p_startPoint,Point p_endPoint)
{
	InitializePathPlanner(p_startPoint);
	pair<Node,Point> cur_node;

	int openList_index;
	while(! m_openList.is_empty())
	{
		cur_node = m_openList.pop();
		m_visited[cur_node.second.Y][cur_node.second.X] = true;
		if(cur_node.second == p_endPoint)
			return m_pathGrid[cur_node.second.Y][cur_node.second.X].getF();
		Point adj_point;
		for(int index=0;index<POSSIBLE_MOVES_SIZE;++index)
		{
			adj_point.X=cur_node.second.X + xDirection[index];
			adj_point.Y=cur_node.second.Y + yDirection[index];
			if(m_occupancyGrid->checkBoundries(adj_point))
			{
				if(m_occupancyGrid->isWalkable(adj_point) && !m_visited[adj_point.Y][adj_point.X])
				{
					int newG = 1 + m_pathGrid[cur_node.second.Y][cur_node.second.X].getG();
					if((openList_index=m_openList.find(make_pair(m_pathGrid[adj_point.Y][adj_point.X],adj_point))) && openList_index >= 0)
					{
						if(newG < m_pathGrid[adj_point.Y][adj_point.X].getG())
						{
							m_pathGrid[adj_point.Y][adj_point.X].setG(newG);
							m_pathGrid[adj_point.Y][adj_point.X].setParent(cur_node.second);
							m_pathGrid[adj_point.Y][adj_point.X].setF(H(adj_point,p_endPoint));
							m_openList.update_element(openList_index,make_pair(m_pathGrid[adj_point.Y][adj_point.X],adj_point));
						}
					}
					else
					{
						m_pathGrid[adj_point.Y][adj_point.X].setG(newG);
						m_pathGrid[adj_point.Y][adj_point.X].setParent(cur_node.second);
						m_pathGrid[adj_point.Y][adj_point.X].setF(H(adj_point,p_endPoint));
						m_openList.push(make_pair(m_pathGrid[adj_point.Y][adj_point.X],adj_point));
					}
				}
			}

		}
	}
	return OO;
}
vector<Point> PathPlanner::Get_Path(Point p_startPoint,Point p_endPoint)
{
	vector<Point> path;
	Point cur_point=p_endPoint;
	while(cur_point != p_startPoint)
	{
		path.push_back(cur_point);
		cur_point = m_pathGrid[cur_point.Y][cur_point.X].getParentNodePoint();
	}
	path.push_back(p_startPoint);
	reverse(path.begin(),path.end());

	return path;
}
vector<Point> PathPlanner::Get_ShortestPath(Point p_startPoint,vector<Point> Goals)
{
	vector<Point> shortest_path;
	int min_cost=1<<30,curPathCost;
	Point newGoal;
	for(int index=0;index < Goals.size();++ index)
	{
		if(m_previousGoal == Goals[index]) // Skip the previous goal/frontier if it is one of the new suggested frontiers
			continue;

		curPathCost = Find_ShortestPath_MIN_Cost(p_startPoint,Goals[index]);
		if(	curPathCost < min_cost)
		{
			min_cost=curPathCost;
			shortest_path =Get_Path(p_startPoint,Goals[index]);
			newGoal = Goals[index];
		}
	}
	m_previousGoal = newGoal; // Copy the new goal in the previous goal variable for skipping it in the next pathfinding process

	return shortest_path;
}
void PathPlanner::Clear()
{
	for(int row_index=0;row_index<m_occupancyGrid->getHeight(); ++ row_index)
	{
		delete[] m_pathGrid[row_index];
		delete[] m_visited[row_index];
	}
	delete m_pathGrid;
	delete m_visited;

	m_openList.clear();
}
PathPlanner::PathPlanner(void)
{
}
PathPlanner::PathPlanner(Evidence_Grid* globalGrid) : m_occupancyGrid(globalGrid)
{
	m_pathGrid = new Node*[m_occupancyGrid->getHeight()];
	m_visited = new bool*[m_occupancyGrid->getHeight()];
	for(int row_index=0;row_index<m_occupancyGrid->getHeight(); ++ row_index)
	{
		m_pathGrid[row_index]=new Node[m_occupancyGrid->getWidth()];
		m_visited[row_index] = new bool[m_occupancyGrid->getWidth()];
	}
}
PathPlanner::PathPlanner(Evidence_Grid* globalGrid,Point p_initialGoal) : m_previousGoal(p_initialGoal)
{
	PathPlanner::PathPlanner(globalGrid);
}
PathPlanner::~PathPlanner(void)
{
	Clear();
}
