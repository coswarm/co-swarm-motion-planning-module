#pragma once
#include<algorithm>
#include<cmath>
#include<map>
#include <limits>
#include <queue>
#include<iostream>
#include<algorithm>
#include <vector>
#include <functional>
#include <assert.h>
using namespace std;

const int xDirection[] = { 1,-1,0,0};
const int yDirection[]={ 0,0,1,-1};
#define POSSIBLE_MOVES_SIZE sizeof(xDirection)/sizeof(xDirection[0])
#define OO 1<<30

struct Point
{
	int X;
	int Y;
	Point(void){}
	Point(int X,int Y)
	{
		this->X=X;
		this->Y=Y;
	}
	Point(const Point &P)
	{
		X=P.X;
		Y=P.Y;
	}
	bool operator==(const Point &P) const
	{
		return X==P.X && Y==P.Y;
	}
	bool operator!=(const Point &P) const
	{
		return !(this->operator==(P));
	}
	bool operator<(const Point &P) const
	{
		if(X==P.X)
			return Y < P.Y;
		return X < P.X;
	}
};
class  Node
{
private:
	int F;
	int G;
	Point m_parentNode;
public:
	Node(void):F(OO),G(OO){}
	Node(int f, int g,Point p):F(f),G(g),m_parentNode(p){}
	~Node(void) {} 

	int getF()const {return F;}
	int getG()const {return G;}
	Point getParentNodePoint()const {return m_parentNode;}
	void setF(int H ) { F=G + H;}
	void setG(int G ) { this->G=G;}
	void setParent(Point parent_point) { m_parentNode = parent_point; }
	void reset() {F=G=OO;}

	bool operator<=(const Node &n) const
	{
		if(F == n.F)
			return G <= n.G;
		return F <= n.F;
	}
	bool operator<(const Node &n) const
	{
		if(F == n.F)
			return G < n.G;
		return F < n.F;
	}
	bool operator==(const Node &n) const
	{
		return n.F==F && n.G==G;
	}
};